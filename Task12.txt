package applicationsuitecrm;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class SeleniumTask12 
{
 
	WebDriver driver;	
	
	  @BeforeClass
	  public void beforeClass() 
	  {
		  driver= new FirefoxDriver();
		  driver.get("http://alchemy.hguy.co/crm");
		  driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		  System.out.println("Page Title is " + driver.getTitle());
	   
	  }
	  
	  @Test
	  public void schedule()
	  {
		     WebElement userName = driver.findElement(By.id("user_name"));
			 WebElement password = driver.findElement(By.id("username_password"));		
			 userName.sendKeys("admin");
			 password.sendKeys("pa$$w0rd");		 	 
			 driver.findElement(By.id("bigbutton")).click();	
			 driver.findElement(By.xpath("//*[@id=\"grouptab_0\"]")).click();
			 WebElement meeting = driver.findElement(By.xpath("//*[@id=\"moduleTab_9_Meetings\"]"));
			 ((JavascriptExecutor) driver).executeScript("arguments[0].click();", meeting);	
			 driver.findElement(By.xpath("//a[@data-action-name='Schedule_Meeting']")).click();
			 WebElement subject = driver.findElement(By.id("name"));
			 WebDriverWait wait = new WebDriverWait(driver, 10);
			 wait.until(ExpectedConditions.elementToBeClickable(subject)).click();
			 subject.sendKeys("Activity 12 Meeting");
			 driver.findElement(By.id("search_first_name")).sendKeys("Allene A");
			 driver.findElement(By.id("invitees_search")).click();
			 driver.findElement(By.id("invitees_add_1")).click();
			 driver.findElement(By.id("search_first_name")).sendKeys("Darlene C");
			 driver.findElement(By.id("invitees_search")).click();
			 driver.findElement(By.id("invitees_add_1")).click();
			 driver.findElement(By.id("save_and_send_invites_header")).click();
			 driver.findElement(By.xpath("//div[contains(text(),'View Meetings')]"));
			 String confirmCreation = driver.findElement(By.xpath("//*[@field='name']")).getText();
			 System.out.println("Name is:" + confirmCreation);
			 Assert.assertEquals(confirmCreation , "Activity 12 Meeting");		 
			 System.out.println("Assertion successful");

	  }
	  
	  @AfterClass
	  public void afterClass() 
	  {
		 //driver.close();  
	  } 
	
}